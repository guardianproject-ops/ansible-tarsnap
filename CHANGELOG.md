# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
### Changed
### Removed

## [0.1.0] - 2020-05-11

### Added

- This CHANGELOG

### Changed

n/a

### Removed

n/a

[Unreleased]: https://gitlab.com/guardianproject-ops/ansible-tarsnap/compare/0.1.0...master
[0.1.0]: https://gitlab.com/guardianproject-ops/ansible-tarsnap/-/tags/0.1.0
