## Role Variables

* `tarsnap_key`: `None` - a variable containing the contents of the machine key. mutually exclusive with `tarsnap_key_local_path`



* `tarsnap_key_local_path`: `None` - the local path to the key file. mutually exclusive with `tarsnap_key`



* `tarsnap_write_only`: `false` - whether the tarsnap key only supports writing (and not reading)



* `tarsnap_backup_pre_script`: `false` - a path to a script to execute before the backup



* `tarsnap_backup_post_script`: `false` - a path to a script to execute after the backup



* `tarsnap_backup_folders`: `''` - a space separated list of absolute paths to backup. this is passed directly to the tarsnap command, so must be all one one line


